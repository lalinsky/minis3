import os, sys
sys.path.append(os.path.dirname(__file__))
import minis3
application = minis3.create_application(os.environ['MINIS3_CONFIG'])
