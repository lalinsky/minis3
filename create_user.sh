#!/usr/bin/env bash

user_id=`openssl rand -hex 8`
user_display_name=$1
access_key_id=`openssl rand -hex 8`
access_key_secret=`openssl rand -base64 32`

echo "  - id: $user_id"
echo "    display_name: \"$user_display_name\""
echo "    access_keys:"
echo "      - id: $access_key_id"
echo "        secret: $access_key_secret"

