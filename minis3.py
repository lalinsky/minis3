import os
import urllib
import shutil
import subprocess
import itertools
import tempfile
import time
import hashlib
import hmac
import hashlib
import yaml
from xattr import getxattr, setxattr
from lxml import etree as ET
from lxml.builder import ElementMaker
from werkzeug.wrappers import Request, Response
from werkzeug.routing import Map, Rule
from werkzeug.wsgi import wrap_file
from werkzeug.exceptions import HTTPException, BadRequest, NotImplemented, NotFound


S3_XATTR_MD5 = 'user.s3.md5'
S3_XATTR_CONTENT_TYPE = 'user.s3.content-type'
S3_XATTR_OWNER = 'user.s3.owner'


S3_NS = "http://s3.amazonaws.com/doc/2006-03-01/"
E = ElementMaker(namespace=S3_NS, nsmap={None: S3_NS})


def make_xml_response(xml, status=200):
    response = Response(ET.tostring(xml, xml_declaration=True, encoding='UTF-8'), status=status)
    response.headers['Content-Type'] = 'application/xml; charset=UTF-8'
    return response


def hash_name(name):
    return hashlib.sha256(name).hexdigest()


def escape_name(name):
    return urllib.quote(name, '')


def unescape_name(name):
    return urllib.unquote(name)


def md5sum(path):
    output = subprocess.check_output(['md5sum', path])
    return output.split()[0]


def ensure_dir_exists(path):
    try:
        os.makedirs(path)
    except OSError as e:
        if e.errno != 17:
            raise


class NamedTemporaryDirectory(object):

    def __init__(self, dir=None):
        self.name = tempfile.mkdtemp(dir=dir)

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        shutil.rmtree(self.name)


class S3ServiceError(Exception):

    def __init__(self, status, code, resource=None, request_id=None):
        self.status = status
        self.code = code
        self.resource = resource
        self.request_id = request_id

    def get_response(self, environ):
        xml = E.Error(
            E.Code(self.code),
            E.Message(self.code),
            E.Resource(self.resource or ''),
            E.RequestId(self.request_id or ''),
        )
        return make_xml_response(xml, self.status)


class S3AccessKey(object):

    def __init__(self, id, secret):
        self.id = id
        self.secret = secret
        self.user = None


class S3User(object):

    def __init__(self, id, display_name):
        self.id = id
        self.display_name = display_name
        self.access_keys = []

    def add_access_key(self, access_key):
        self.access_keys.append(access_key)
        access_key.user = self


class S3Users(object):

    def __init__(self):
        self.users = []

    def add_user(self, user):
        self.users.append(user)

    def lookup_user(self, user_id):
        for user in self.users:
            if user.id == user_id:
                return user

    def lookup_access_key(self, access_key_id):
        for user in self.users:
            for access_key in user.access_keys:
                if access_key.id == access_key_id:
                    return access_key


class S3Object(object):

    def __init__(self, bucket, name):
        self.bucket = bucket
        self.name = name
        h = hash_name(name)
        self.dir = os.path.join(self.bucket.dir, h[0:2], h[2:4])
        self.file_name = escape_name(name)
        self.file_path = os.path.join(self.dir, self.file_name)

    @property
    def etag(self):
        return '"{0}"'.format(self.md5)

    @property
    def md5(self):
        return getxattr(self.file_path, S3_XATTR_MD5)

    @property
    def content_type(self):
        return getxattr(self.file_path, S3_XATTR_CONTENT_TYPE)

    @property
    def owner(self):
        return getxattr(self.file_path, S3_XATTR_OWNER)

    @property
    def size(self):
        return os.lstat(self.file_path).st_size

    @property
    def last_modified(self):
        return os.lstat(self.file_path).st_mtime

    def exists(self):
        return os.path.exists(self.file_path)

    def upload(self, stream, content_type, owner):
        ensure_dir_exists(self.dir)
        with self.bucket.storage.create_temp_dir() as temp_dir:
            temp_file_path = os.path.join(temp_dir.name, 'upload')
            with open(temp_file_path, 'w') as temp_file:
                shutil.copyfileobj(stream, temp_file)
            md5 = md5sum(temp_file_path)
            setxattr(temp_file_path, S3_XATTR_MD5, md5)
            setxattr(temp_file_path, S3_XATTR_CONTENT_TYPE, content_type)
            setxattr(temp_file_path, S3_XATTR_OWNER, owner)
            os.rename(temp_file_path, self.file_path)
            return md5

    def download(self):
        return open(self.file_path, 'r')


class S3Bucket(object):

    def __init__(self, storage, name):
        self.storage = storage
        self.name = name
        self.dir = os.path.join(self.storage.data_dir, hash_name(name)[:2], escape_name(name))

    @property
    def owner(self):
        return getxattr(self.dir, S3_XATTR_OWNER)

    @property
    def created(self):
        return os.lstat(self.dir).st_ctime

    def exists(self):
        return os.path.exists(self.dir)

    def get_object(self, object_name):
        return S3Object(self, object_name)

    def create(self, owner):
        ensure_dir_exists(self.dir)
        setxattr(self.dir, S3_XATTR_OWNER, owner)
        return True

    def is_empty(self):
        for object in self.iter_objects():
            return False
        return True

    def delete(self):
        shutil.rmtree(self.dir)

    def iter_objects(self):
        if not self.exists():
            return
        for part1 in os.listdir(self.dir):
            for part2 in os.listdir(os.path.join(self.dir, part1)):
                for escaped_name in os.listdir(os.path.join(self.dir, part1, part2)):
                    yield S3Object(self, unescape_name(escaped_name))


class S3Storage(object):

    def __init__(self, dir):
        self.dir = dir
        self.temp_dir = os.path.join(self.dir, 'temp')
        self.data_dir = os.path.join(self.dir, 'data')

    def exists(self):
        return os.path.exists(self.dir)

    def create_temp_dir(self):
        ensure_dir_exists(self.temp_dir)
        return NamedTemporaryDirectory(self.temp_dir)

    def get_bucket(self, bucket_name):
        return S3Bucket(self, bucket_name)

    def iter_buckets(self):
        if not self.exists():
            return
        for part in os.listdir(self.data_dir):
            for escaped_name in os.listdir(os.path.join(self.data_dir, part)):
                yield S3Bucket(self, unescape_name(escaped_name))


class S3Application(object):

    def __init__(self, host, storage, users, accelerate):
        self.host = host
        self.storage = storage
        self.users = users
        self.accelerate = accelerate

    def format_ts(self, ts):
        return time.strftime('%Y-%m-%dT%H:%M:%S.000Z', time.gmtime(ts))

    def handle_get_service(self, request):
        buckets_xml = []
        for bucket in self.storage.iter_buckets():
            if bucket.owner != self.user.id:
                continue
            buckets_xml.append(
                E.Bucket(
                    E.Name(bucket.name),
                    E.CreationDate(self.format_ts(bucket.created)),
                )
            )

        xml = E.ListAllMyBucketsResult(
            E.Owner(
                E.ID(self.user.id),
                E.DisplayName(self.user.display_name),
            ),
            E.Buckets(*buckets_xml),
        )
        return make_xml_response(xml)

    def handle_get_bucket(self, request, bucket_name):
        bucket = self.storage.get_bucket(bucket_name)
        if not bucket.exists():
            raise S3ServiceError(404, 'NoSuchBucket')

        if bucket.owner != self.user.id:
            raise S3ServiceError(403, 'AccessDenied')

        prefix = request.args.get('prefix', default='')
        max_keys = request.args.get('max-keys', type=int, default=1000)

        objects_xml = []
        is_truncated = False
        for object in bucket.iter_objects():
            if len(objects_xml) == max_keys:
                is_truncated = True
                break
            if bucket.owner != self.user.id:
                continue
            if prefix and not object.name.startswith(prefix):
                continue
            owner = self.users.lookup_user(object.owner)
            objects_xml.append(
                E.Contents(
                    E.Key(object.name),
                    E.LastModified(self.format_ts(object.last_modified)),
                    E.ETag(object.etag),
                    E.Size(str(object.size)),
                    E.StorageClass('STANDARD'),
                    E.Owner(
                        E.ID(owner.id),
                        E.DisplayName(owner.display_name),
                    )
                )
            )

        xml = E.ListBucketResult(
            E.Name(bucket.name),
            E.Prefix(prefix),
            E.Marker(),
            E.MaxKeys(str(max_keys)),
            E.IsTruncated('true' if is_truncated else 'false'),
            *objects_xml
        )
        return make_xml_response(xml)

    def handle_head_bucket(self, request, bucket_name):
        bucket = self.storage.get_bucket(bucket_name)
        if not bucket.exists():
            raise S3ServiceError(404, 'NoSuchBucket')

        if bucket.owner != self.user.id:
            raise S3ServiceError(403, 'AccessDenied')

        return Response()

    def handle_put_bucket(self, request, bucket_name):
        bucket = self.storage.get_bucket(bucket_name)
        if bucket.exists():
            if bucket.owner == self.user.id:
                raise S3ServiceError(409, 'BucketAlreadyOwnedByYou')
            else:
                raise S3ServiceError(409, 'BucketAlreadyExists')

        bucket.create(self.user.id)

        return Response()

    def handle_delete_bucket(self, request, bucket_name):
        bucket = self.storage.get_bucket(bucket_name)
        if not bucket.exists():
            raise S3ServiceError(404, 'NoSuchBucket')

        if bucket.owner != self.user.id:
            raise S3ServiceError(403, 'AccessDenied')

        if not bucket.is_empty():
            raise S3ServiceError(409, 'BucketNotEmpty')

        bucket.delete()
        return Response('', 204)

    def handle_head_object(self, request, bucket_name, object_name):
        bucket = self.storage.get_bucket(bucket_name)
        if not bucket.exists():
            raise S3ServiceError(404, 'NoSuchBucket')

        if bucket.owner != self.user.id:
            raise S3ServiceError(403, 'AccessDenied')

        object = bucket.get_object(object_name)
        if not object.exists():
            raise S3ServiceError(404, 'NoSuchKey')

        if object.owner != self.user.id:
            raise S3ServiceError(403, 'AccessDenied')

        response = Response()
        response.headers['ETag'] = object.etag
        response.headers['Content-Type'] = object.content_type
        response.headers['Content-Length'] = object.size
        return response

    def handle_get_object(self, request, bucket_name, object_name):
        bucket = self.storage.get_bucket(bucket_name)
        if not bucket.exists():
            raise S3ServiceError(404, 'NoSuchBucket')

        if bucket.owner != self.user.id:
            raise S3ServiceError(403, 'AccessDenied')

        object = bucket.get_object(object_name)
        if not object.exists():
            raise S3ServiceError(404, 'NoSuchKey')

        if object.owner != self.user.id:
            raise S3ServiceError(403, 'AccessDenied')

        if self.accelerate == 'x-accel-redirect':
            url = '/protected/' + object.file_path[len(self.storage.dir):].lstrip('/')
            response = Response()
            response.headers['X-Accel-Redirect'] = url
        elif self.accelerate == 'x-sendfile':
            response = Response()
            response.headers['X-Sendfile'] = object.file_path
        else:
            stream = wrap_file(request.environ, object.download())
            response = Response(stream, direct_passthrough=True)

        response.headers['ETag'] = object.etag
        response.headers['Content-Type'] = object.content_type
        response.headers['Content-Length'] = object.size
        return response

    def handle_put_object(self, request, bucket_name, object_name):
        bucket = self.storage.get_bucket(bucket_name)
        if not bucket.exists():
            raise S3ServiceError(404, 'NoSuchBucket')

        if bucket.owner != self.user.id:
            raise S3ServiceError(403, 'AccessDenied')

        object = bucket.get_object(object_name)
        object.upload(request.stream, request.content_type, self.user.id)

        response = Response()
        response.headers['ETag'] = object.etag
        return response

    def handle_delete_object(self, request, bucket_name, object_name):
        bucket = self.storage.get_bucket(bucket_name)
        if not bucket.exists():
            raise S3ServiceError(404, 'NoSuchBucket')

        if bucket.owner != self.user.id:
            raise S3ServiceError(403, 'AccessDenied')

        object = bucket.get_object(object_name)
        if not object.exists():
            raise S3ServiceError(404, 'NoSuchKey')

        if object.owner != self.user.id:
            raise S3ServiceError(403, 'AccessDenied')

        object.delete()
        return Response('', 204)

    def get_string_to_sign(self, request, expires, resource):
        result = request.method + '\n'
        result += request.headers.get('Content-MD5', '') + '\n'
        result += request.headers.get('Content-Type', '') + '\n'
        if expires:
            result += str(expires) + '\n'
        else:
            result += request.headers.get('Date', '') + '\n'
        for name, value in request.headers.iteritems():
            name = name.lower()
            if name.startswith('x-amz-'):
                result += '{0}:{1}\n'.format(name, value)
        result += resource
        return result

    def sign_string(self, secret, string_to_sign):
        if isinstance(string_to_sign, unicode):
            string_to_sign = string_to_sign.encode('utf-8')
        signature = hmac.new(secret, string_to_sign, hashlib.sha1).digest()
        return signature.encode('base64').strip()

    def authenticate(self, request, resource, access_key_id, signature, expires=''):
        access_key = self.users.lookup_access_key(access_key_id)
        if not access_key:
            raise S3ServiceError(403, 'InvalidAccessKeyId')

        string_to_sign = self.get_string_to_sign(request, expires, resource)
        expected_signature = self.sign_string(access_key.secret, string_to_sign)

        if signature != expected_signature:
            raise S3ServiceError(403, 'SignatureDoesNotMatch')

        if expires != '' and expires <= time.time():
            raise S3ServiceError(400, 'ExpiredToken')

        self.user = access_key.user

    def authenticate_request_by_header(self, request, resource):
        authentication = request.headers.get('Authorization')
        if authentication:
            method, _, auth_string = authentication.partition(' ')
            if method == 'AWS':
                access_key_id, _, signature = auth_string.partition(':')
                self.authenticate(request, resource, access_key_id, signature)
                return True

    def authenticate_request_by_query_string(self, request, resource):
        access_key_id = request.args.get('AWSAccessKeyId')
        signature = request.args.get('Signature')
        expires = request.args.get('Expires', type=int)
        if access_key_id and signature and expires:
            self.authenticate(request, resource, access_key_id, signature, expires)
            return True

    def authenticate_request(self, request, resource, subresource):
        if self.authenticate_request_by_header(request, resource):
            return True

        if self.authenticate_request_by_query_string(request, resource):
            return True

        raise S3ServiceError(403, 'AccessDenied')

    def get_bucket_name(self, request):
        host = request.headers.get('Host').split(':')[0]
        if host and host.endswith('.' + self.host):
            return host[:-len(self.host)-1]

    def get_object_name(self, request, bucket_name):
        object_name = request.path.lstrip('/')
        if not bucket_name:
            host = request.headers.get('Host').split(':')[0]
            if not host:
                bucket_name, object_name = object_name.split('/', 1)
            else:
                bucket_name = host.lower()
        return bucket_name, object_name

    def handle(self, request):
        bucket_name = self.get_bucket_name(request)
        if request.path == '/':
            if bucket_name:
                resource = '/' + bucket_name + '/'
                handler_type = 'bucket'
                handler_args = [bucket_name]
            else:
                resource = '/'
                handler_type = 'service'
                handler_args = []
        else:
            bucket_name, object_name = self.get_object_name(request, bucket_name)
            resource = '/' + bucket_name + '/' + object_name
            handler_type = 'object'
            handler_args = [bucket_name, object_name]

        method = request.method.lower()
        handler_name = 'handle_{0}_{1}'.format(method, handler_type)

        subresource = ''
        for name, values in request.args.iterlists():
            if values == ['']:
                subresource = name

        if subresource:
            resource += '?' + subresource
            handler_name += '_' + subresource

        try:
            handler = getattr(self, handler_name, None)
            if handler is None:
                raise S3ServiceError(501, 'NotImplemented')
            self.authenticate_request(request, resource, subresource)
            return handler(request, *handler_args)
        except S3ServiceError as e:
            e.resource = resource
            e.request_id = str(id(request))
            raise e

    def __call__(self, environ, start_response):
        request = Request(environ)
        try:
            response = self.handle(request)
        except S3ServiceError as e:
            response = e.get_response(request.environ)
        except HTTPException as e:
            response = e.get_response(request.environ)
        response.headers['Server'] = 'MiniS3'
        return response(environ, start_response)


def create_application(config_path):
    config = yaml.load(open(config_path))
    host = config['host']
    storage = S3Storage(config['storage_dir'])
    users = S3Users()
    for user_data in config['users']:
        user = S3User(user_data['id'], user_data['display_name'])
        for access_key_data in user_data['access_keys']:
            access_key = S3AccessKey(access_key_data['id'], access_key_data['secret'])
            user.add_access_key(access_key)
        users.add_user(user)
    accelerate = config['accelerate']
    return S3Application(host, storage, users, accelerate)


if __name__ == '__main__':
    import sys
    from werkzeug.serving import run_simple
    application = create_application(sys.argv[1])
    run_simple('localhost', 4000, application)

