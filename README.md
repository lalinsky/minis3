Tiny AWS S3-like Service for Development
----------------------------------------

Configuration and starting the service up:

    $ apt-get install python-yaml python-xattr python-lxml python-werkzeug
    $ cp minis3.conf.example minis3.conf
    $ vim minis3.conf
    $ python minis3.py minis3.conf

